import 'package:flutter/material.dart';
import 'package:sqlite_database/models/todo.dart';
import 'package:sqlite_database/service/todo_service.dart';

class TodoByCategory extends StatefulWidget {
  final String category;

  TodoByCategory({this.category});

  @override
  _TodoByCategoryState createState() => _TodoByCategoryState();
}

class _TodoByCategoryState extends State<TodoByCategory> {
  List<Todo> _todoList = List<Todo>();
  TodoService _todoService = TodoService();

  getTodosByCategories() async {
    var todos = await _todoService.readTodoByCategory(this.widget.category);

    todos.forEach((todo) {
      setState(() {
        var model = Todo();
        model.title = todo['title'];
        model.description = todo['description'];
        model.todoDate = todo['todoDate'];
        model.category = todo['category'];

        _todoList.add(model);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getTodosByCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.category),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: _todoList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:
                        const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Card(
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[Text('${_todoList[index].title}')],
                        ),
                        subtitle: Text('${_todoList[index].category}'),
                        trailing: Text('${_todoList[index].todoDate}'),
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
