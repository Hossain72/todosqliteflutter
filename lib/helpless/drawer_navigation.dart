import 'package:flutter/material.dart';
import 'package:sqlite_database/helpless/todo_by_category.dart';
import 'package:sqlite_database/screen/categories_screen.dart';
import 'package:sqlite_database/screen/home_screen.dart';
import 'package:sqlite_database/service/category_service.dart';

class DrawerNavigation extends StatefulWidget {
  @override
  _DrawerNavigationState createState() => _DrawerNavigationState();
}

class _DrawerNavigationState extends State<DrawerNavigation> {
  List<Widget> _categoryList = List<Widget>();
  CategoryService _categoryService = CategoryService();

  getAllCategories() async {
    var categories = await _categoryService.readCategories();

    categories.forEach((category) {
      setState(() {
        _categoryList.add(InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => TodoByCategory(
                      category: category['name'],
                    )));
          },
          child: ListTile(
            title: Text(category['name']),
          ),
        ));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getAllCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://scontent.fdac15-1.fna.fbcdn.net/v/t1.0-9/70450238_1091212141083056_6096161766876643328_o.jpg?_nc_cat=104&_nc_sid=85a577&_nc_ohc=QbCms-Bc4MkAX81T0zk&_nc_ht=scontent.fdac15-1.fna&oh=acd81ccab1939a12ddec8486df9dbcbb&oe=5F8417AD'),
              ),
              accountName: Text("Anowar Hossain"),
              accountEmail: Text("hossainanowar72@gmail.com"),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text("Home"),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
            ),
            ListTile(
              leading: Icon(Icons.view_list),
              title: Text("Categories"),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => CategoriesScreen()));
              },
            ),
            Divider(),
            Column(
              children: _categoryList,
            )
          ],
        ),
      ),
    );
  }
}
