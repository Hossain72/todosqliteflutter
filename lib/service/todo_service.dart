import 'package:sqlite_database/models/todo.dart';
import 'package:sqlite_database/repositories/repository.dart';

class TodoService {
  Repository _repository;

  TodoService() {
    _repository = Repository();
  }

  saveTodo(Todo todo) async {
    return _repository.insetData("todos", todo.todoMap());
  }

  readTodo() async {
    return _repository.readData("todos");
  }

  readTodoByCategory(category) async {
    return _repository.readDataByColumnName('todos', 'category', category);
  }
}
